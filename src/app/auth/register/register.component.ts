import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotifyService } from 'src/app/helper/notify.service';
import { User } from '../interfaces/user';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  formSignUp = this.fb.group(
    {
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password1: ['', [Validators.required, Validators.minLength(6)]],
      password2: ['', [Validators.required, Validators.minLength(6)]],
    },
    { validator: this.matchingPasswords }
  );

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private notifyService: NotifyService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  matchingPasswords(group: FormGroup) {
    if (group) {
      const password1 = group.controls['password1'].value;
      const password2 = group.controls['password2'].value;

      if (password1 == password2) {
        return null;
      }
    }
    return { matching: false };
  }

  onSubmit() {
    let u: User = {
      ...this.formSignUp.value,
      password: this.formSignUp.value.password1,
    };
    this.authService.signup(u).subscribe(
      (u) => {
        this.notifyService.notify('Cadastro realizado com Sucesso!');
        this.router.navigateByUrl('/auth/login');
      },
      (err) => {
        console.error(err);
        this.notifyService.notify(err.error.message);
      }
    );
  }
}

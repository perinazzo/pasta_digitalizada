export interface User {
  name: string;
  email: string;
  password: string;
  token: string;
  _id?: string;
}

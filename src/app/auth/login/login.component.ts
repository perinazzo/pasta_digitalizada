import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotifyService } from 'src/app/helper/notify.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private notifyService: NotifyService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    const credentials = this.loginForm.value;
    this.authService.login(credentials).subscribe(
      (user) => {
        this.notifyService.notify(
          'Login Realizado com Sucesso ' + user.name + ' !'
        );
        this.router.navigateByUrl('/');
      },
      (err) => {
        this.notifyService.notify(err.error.message);
      }
    );
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Credential } from '../interfaces/credential';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  readonly url = 'http://localhost:3000/auth';

  private subjectUser$: BehaviorSubject<User> = new BehaviorSubject(null);
  private subjectLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private http: HttpClient) {}

  signup(user: User): Observable<User> {
    return this.http.post<User>(`${this.url}/signup`, user);
  }

  login(credentials: Credential): Observable<User> {
    return this.http.post<User>(`${this.url}/login`, credentials).pipe(
      tap((u: User) => {
        console.log(u)
        localStorage.setItem('token', u.token);
        this.subjectLoggedIn$.next(true);
        this.subjectUser$.next(u);
      })
    );
  }

  isAuthenticated(): Observable<boolean> {
    return this.subjectLoggedIn$.asObservable();
  }

  getUser(): Observable<User> {
    return this.subjectUser$.asObservable();
  }

  logout() {
    localStorage.removeItem('token');
    this.subjectLoggedIn$.next(false);
    this.subjectUser$.next(null);
  }
}

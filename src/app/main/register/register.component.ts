import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { Register } from '../interfaces/register';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  registers: Register[];
  regiEdit: Register = null;
  file64: any;
  files;
  private unSubscribe$: Subject<any> = new Subject();

  @ViewChild('form') from: NgForm;

  registerForm = this.fb.group({
    title: ['', [Validators.required]],
    content: [''],
  });

  constructor(
    private registerService: RegisterService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.registerService
      .getRegister()
      .pipe(

        takeUntil(this.unSubscribe$)
      )
      .subscribe(
        (registers) => {
          this.registers = registers;
        },
        (err) => {
          console.error(err);
        }
      );
      
  }

  generateFile(file) {
    console.log(file != null)
    if (file != null) {
      const pdfInBase64 = file;
      const newBlob = new Blob([pdfInBase64], { type: 'application/pdf' });
    
      return newBlob;
    }
    
  }

  onFileChanges(e) {
    
    this.registerForm.controls['content'].setValue(e[0].name)
    this.file64 = e[0].base64;
   
  }


  submit() {
    if (this.regiEdit == null) {
      this.registerService
        .addRegister({
          title: this.registerForm.value.title,
          content: this.registerForm.value.content,
          file: this.file64,
        })
        .subscribe(
          (register) => {
            this.notify('Documento Salvo com Sucesso!');
          },
          (err) => {
            console.error(err);
            this.notify('Erro Interno ao Salvar!');
          }
        );
    } else {
      this.registerService
        .editRegister({
          title: this.registerForm.value.title,
          content: this.registerForm.value.content,
          file: '',
          _id: this.regiEdit._id,
        })
        .subscribe(
          (regi) => {
            this.notify('Documento Atualizado com Sucesso!');
          },
          (err) => {
            this.notify('Erro ao Tentar Atualizar Documento!');
          }
        );
    }

    this.clearFields();
  }

  edit(regi: Register) {
    this.registerForm.setValue({
      title: regi.title,
      content: regi.content,
      file: regi.file,
    });
    this.regiEdit = regi;
  }

  delete(regi: Register) {
    this.registerService.deleteRegister(regi).subscribe(
      () => {
        this.notify('Excluído com Sucesso!');
      },
      (err) => {
        this.notify('Erro ao tentar Excluir!');
        console.error(err);
      }
    );
  }

  clearFields() {
    this.from.resetForm();
    this.files = null;
  }

  notify(msg: string) {
    this.snackBar.open(msg, 'OK', { duration: 3000 });
  }

 

  ngOnDestroy() {
    this.unSubscribe$.next();
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  // {
  //   path: '', redirectTo: 'regir'
  // },
  // {
  //   path: 'documents', component: RegisterComponent
  // }
  {
    path: '', redirectTo: 'registers'
  },
  {
    path: 'registers', component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }

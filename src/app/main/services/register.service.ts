import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Register } from '../interfaces/register';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  readonly url = 'http://localhost:3000/api/registers';
  private registersSubject$: BehaviorSubject<Register[]> = new BehaviorSubject<
    Register[]
  >(null);

  constructor(private http: HttpClient) {}

  getRegister(): Observable<Register[]> {
    this.http
      .get<Register[]>(this.url)
      .pipe(tap((registers) => console.log(registers)))
      .subscribe(this.registersSubject$);
    return this.registersSubject$.asObservable();
  }

  addRegister(register: Register): Observable<Register> {
    return this.http
      .post<Register>(this.url, register)
      .pipe(
        tap((newRegister) =>
          this.registersSubject$.getValue().push(newRegister)
        )
      );
  }

  editRegister(register: Register): Observable<Register> {
    return this.http
      .patch<Register>(`${this.url}/${register._id}`, register)
      .pipe(
        tap((regi) => {
          let currentRegisters = this.registersSubject$.getValue();

          let i = currentRegisters.findIndex((r) => r._id == regi._id);
          if (i >= 0) {
            currentRegisters[i].title = regi.title;
            currentRegisters[i].content = regi.content;
          }
        })
      );
  }

  deleteRegister(register: Register): Observable<any> {
    return this.http.delete(`${this.url}/${register._id}`).pipe(
      tap(() => {
        let currentRegisters = this.registersSubject$.getValue();

        let i = currentRegisters.findIndex((r) => r._id == register._id);
        if (i >= 0) {
          currentRegisters.splice(i, 1);
        }
      })
    );
  }
}

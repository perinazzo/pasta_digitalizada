export interface Register {
    title: string;
    content: string;
    file: string;
    _id?: string;
}
